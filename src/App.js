import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

const url = "http://localhost:8080/back-test/rest/movies/";

class App extends Component {
	state = {
		data: [],
		modalInsertar: false,
		modalEliminar: false,
		form: {
			movieId: "",
			title: "",
			year: "",
			duration: "",
		},
	};

	peticionGet = () => {
		axios
			.get(url)
			.then((response) => {
				this.setState({ data: response.data });
				console.log(response.data);
			})
			.catch((error) => {
				console.log(error.message);
			});
	};

	peticionPost = async () => {
		delete this.state.form.movieId;
		console.log(this.state.form);
		await axios
			.post(url, this.state.form)
			.then((response) => {
				this.modalInsertar();
				this.peticionGet();
			})
			.catch((error) => {
				console.log(error.message);
			});
	};

	peticionPut = () => {
		axios
			.put(url + this.state.form.movieId, this.state.form)
			.then((response) => {
				this.modalInsertar();
				this.peticionGet();
			});
	};

	peticionDelete = () => {
		console.log(this.state.form.movieId);
		axios.delete(url + this.state.form.movieId).then((response) => {
			this.setState({ modalEliminar: false });
			this.peticionGet();
		});
	};

	modalInsertar = () => {
		this.setState({ modalInsertar: !this.state.modalInsertar });
	};

	seleccionarMovie = (movie) => {
		this.setState({
			tipoModal: "actualizar",
			form: {
				movieId: movie.movieId,
				title: movie.title,
				year: movie.year,
				duration: movie.duration,
			},
		});
	};

	handleChange = async (e) => {
		e.persist();
		await this.setState({
			form: {
				...this.state.form,
				[e.target.name]: e.target.value,
			},
		});
		console.log(this.state.form);
	};

	componentDidMount() {
		this.peticionGet();
	}

	render() {
		const { form } = this.state;

		return (
			<div className="App">
				<br />
				<br />
				<br />
				<button
					className="btn btn-success"
					onClick={() => {
						this.setState({ form: null, tipoModal: "insertar" });
						this.modalInsertar();
					}}
				>
					Agregar movie
				</button>
				<br />
				<br />
				<table className="table ">
					<thead>
						<tr>
							<th>ID</th>
							<th>Título</th>
							<th>Año</th>
							<th>Duración</th>
							<th>Editar/Eliminar</th>
						</tr>
					</thead>
					<tbody>
						{this.state.data.map((movie) => {
							return (
								<tr key={movie.movieId}>
									<td>{movie.movieId}</td>
									<td>{movie.title}</td>
									<td>{movie.year}</td>
									<td>{movie.duration}</td>
									<td>
										<button
											className="btn btn-primary"
											onClick={() => {
												this.seleccionarMovie(movie);
												this.modalInsertar();
											}}
										>
											<FontAwesomeIcon icon={faEdit} />
										</button>
										{"   "}
										<button
											className="btn btn-danger"
											onClick={() => {
												this.seleccionarMovie(movie);
												this.setState({ modalEliminar: true });
											}}
										>
											<FontAwesomeIcon icon={faTrashAlt} />
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>

				<Modal isOpen={this.state.modalInsertar}>
					<ModalHeader style={{ display: "block" }}>
						<span
							style={{ float: "right" }}
							onClick={() => this.modalInsertar()}
						>
							x
						</span>
					</ModalHeader>
					<ModalBody>
						<div className="form-group">
							{this.state.tipoModal == "insertar" ? (
								<input type="hidden" />
							) : (
								<label htmlFor="id">ID</label>
							)}

							{this.state.tipoModal == "insertar" ? (
								<input type="hidden" />
							) : (
								<input
									className="form-control"
									type="text"
									name="movieId"
									id="movieId"
									readOnly
									onChange={this.handleChange}
									value={form ? form.movieId : this.state.data.length + 1}
								/>
							)}
							<br />
							<label htmlFor="title">Título</label>
							<input
								className="form-control"
								type="text"
								name="title"
								id="title"
								onChange={this.handleChange}
								value={form ? form.title : ""}
							/>
							<br />
							<label htmlFor="year">Año</label>
							<input
								className="form-control"
								type="text"
								name="year"
								id="year"
								onChange={this.handleChange}
								value={form ? form.year : ""}
							/>
							<br />
							<label htmlFor="duration">Duración</label>
							<input
								className="form-control"
								type="text"
								name="duration"
								id="duration"
								onChange={this.handleChange}
								value={form ? form.duration : ""}
							/>
						</div>
					</ModalBody>

					<ModalFooter>
						{this.state.tipoModal == "insertar" ? (
							<button
								className="btn btn-success"
								onClick={() => this.peticionPost()}
							>
								Insertar
							</button>
						) : (
							<button
								className="btn btn-primary"
								onClick={() => this.peticionPut()}
							>
								Actualizar
							</button>
						)}
						<button
							className="btn btn-danger"
							onClick={() => this.modalInsertar()}
						>
							Cancelar
						</button>
					</ModalFooter>
				</Modal>

				<Modal isOpen={this.state.modalEliminar}>
					<ModalBody>
						Estás seguro que deseas eliminar a la movie {form && form.title}
					</ModalBody>
					<ModalFooter>
						<button
							className="btn btn-danger"
							onClick={() => this.peticionDelete()}
						>
							Sí
						</button>
						<button
							className="btn btn-secundary"
							onClick={() => this.setState({ modalEliminar: false })}
						>
							No
						</button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
export default App;
